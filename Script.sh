#!/bin/bash

############################################################
#
#  Description : déploiement à la volée de conteneur docker
#
#  Auteur : Jonas
#
#  Date : 12/12/2020
#
###########################################################


#si option --create
if [ "$1" == "--create" ];then

	#definition du nombre de conteneur 
	nb_machine=1
	[ "$2" != "" ] && nb_machine=$2

	#Recupération de l'ID Max && Setiing min/max
	min=1
	max=0
	idmax=`docker ps -a --format '{{ .Names }}' | awk -F "-" -v user="$USER" '$0 ~ user"-alpine" {print $3}' | sort -r |head -1`
	min=$(($idmax +1 ))
	max=$(($idmax + $nb_machine))
	

	#creation des conteneurs
	echo "Debut de la Creation des Conteneurs"
	for i in $(seq 1 $min $max);do
	  docker run -tid --name $USER-alpine-$i alpine:latest
	  echo "Conteneur $USER-alpine-$i crée"
 	done
	echo "j'ai créé ${nb_machine} "
	

# si option --drop
elif [ "$1" == "--drop" ];then

	echo "Supression Des conteneurs ...."
	docker rm -f $(docker ps -a |grep $USER-alpine |awk '{print $1}')
	echo " Supression Terminé "

# si option --start
elif [ "$1" == "--start" ];then
  
  	echo ""
	docker start $(docker ps -a | grep $USER-alpine | awk '{print $1}')
	echo ""
        
# si option --ansible
elif [ "$1" == "--ansible" ];then
  
  	echo ""
	echo " notre option est --ansible"
	echo ""

# si option --infos
elif [ "$1" == "--infos" ];then
	echo""
      	echo"Inofmrations des conteneurs:"
	for conteneur in $(docker ps -a | grep $USER-alpine | awk '{print $1}');do
	 docker inspect -f ' => {{.Name}} - {{.NetworkSettings.IPAddress }}' $conteneur
 	done
	echo""	

# si aucune option affichage de l'aide
else

echo "

Options :
		- --create : lancer des conteneurs

		- --drop : supprimer les conteneurs créer par le deploy.sh
	
		- --infos : caractéristiques des conteneurs (ip, nom, user...)

		- --start : redémarrage des conteneurs

		- --ansible : déploiement arborescence ansible

"

fi

